.. Progdupeupl documentation master file, created by
   sphinx-quickstart on Sat Dec 07 17:25:18 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Progdupeupl's documentation!
=======================================

Contents:

.. toctree::
    :maxdepth: 2

    article
    tutorial
    forum

    member
    messages
    gallery

    pages
    utils

.. automodule:: pdp

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

